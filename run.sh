#!/bin/bash

set -eux

cat schutzbot/team_ssh_keys.txt | tee -a ~/.ssh/authorized_keys > /dev/null

dnf install -y dnf-plugins-core
dnf copr enable -y @osbuild/osbuild-composer
dnf install -y osbuild-composer lorax weldr-client rsync

systemctl enable --now osbuild-composer.socket

tee "bp.toml" >/dev/null <<EOF
name = "bp"
distro = "fedora-38"
EOF

composer-cli blueprints push bp.toml
composer-cli --json compose start bp image-installer >compose-status.json

COMPOSE_ID=$(jq -r '.[0].body.build_id' compose-status.json)

while true; do
  sudo composer-cli --json compose info "${COMPOSE_ID}" | tee compose-info.json >/dev/null
  COMPOSE_STATUS=$(jq -r '.[0].body.queue_status' compose-info.json)

  # Is the compose finished?
  if [[ $COMPOSE_STATUS != RUNNING ]] && [[ $COMPOSE_STATUS != WAITING ]]; then
    break
  fi

  # Wait 30 seconds and try again.
  sleep 30
done

if [[ $COMPOSE_STATUS != FINISHED ]]; then
    echo "Something went wrong with the compose. 😢"
    composer-cli compose logs "$COMPOSE_ID"
    tar xf "$COMPOSE_ID-logs.tar"
    cat logs/*
    exit 1
fi

FILENAME="${COMPOSE_ID}-installer.iso"

composer-cli compose image "${COMPOSE_ID}"

curl -Lo liveimg.tar.gz https://fedorapeople.org/groups/anaconda/webui_new_payload/f37-ga-workstation.tar.gz

RESULT="fedora-preview-installer-$(date --iso-8601).iso"
mkksiso -a liveimg.tar.gz "$FILENAME" "$RESULT"

if [[ "${CI_PIPELINE_SOURCE:-}" == "schedule" || "${CI_PIPELINE_SOURCE:-}" == "web" ]]; then
  mkdir -p ~/.ssh
  ssh-keyscan fedorapeople.org >>~/.ssh/known_hosts
  rsync \
    --links \
    --rsh "ssh -i \"$IMAGEBUILDER_BOT_SSH_KEY\"" \
    "$RESULT" \
    imagebuilder-bot@fedorapeople.org:/project/anaconda/webui_preview_image/x86_64/
fi
